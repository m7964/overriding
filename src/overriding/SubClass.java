package overriding;

public class SubClass extends Application {
	
	/**
	 *1. 메서드 이름 변경하면 에러 발생 o
	 *2. 메소드의 리턴타입 변경하면 에러 o
	 *3. 매개변수 갯수,타입이 변경되면 에러 o
	 */
	@Override
	public void method(int num) {}
	
	
	// 4. private 메소드는 오버라이딩 불가
	@Override
	private void privateMethod() {}
	
	// 5. final 오버라이딩 불가
	@Override
	public final void finalMethod() {}
	
	
	// 6.부모 메소드의 접근제한자와 같거나 더 넓은 범위로 오바라이딩 가능 // protected -> 디폴트 낮은범위 불가능
	@Override
	void protectedMethod() {}
	
	
	@Override
	protected void protecteMethod() {}	// 같은 범위 가능
	
	
	@Override
	public void protectedMethod() {} // 더 넓은 범위 가능
	
	
	// 7. Application에 final를 클래스에 추가하면 상속 자체가 안된다. 
	
	

}























