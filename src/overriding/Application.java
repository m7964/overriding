package overriding;

public class Application {
	
	
	public void method(int num) {}
	
	private void privateMethod() {}
	
	public final void finalMethod() {}
	
	protected void protecteMethod() {}
	

}
